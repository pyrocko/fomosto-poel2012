# POEL (packaged as Fomosto backend)

Code for calculating synthetic displacements due to excess pore pressure and
displacements in a multilayered half-space.

POEL has been written by Rongjiang Wang.

Packaging has been done by Hannes Vasyura-Bathke.

## References

- Wang, R. and Kümpel, H.-J.: Poroelasticity: Efficient modeling of strongly
  coupled, slow deformation processes in a multilayered half-space,
  Geophysics, 68, 705–717, 2003

## Compile and install

```
autoreconf -i   # only if 'configure' script is missing
./configure
make
sudo make install
```
